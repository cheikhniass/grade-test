package com.example.controlller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cheikhniass on 1/17/16.
 */
@RestController
public class Controller {
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<String> hello(){
        return  new ResponseEntity<String>("hell Tj", HttpStatus.OK);
    }
}
